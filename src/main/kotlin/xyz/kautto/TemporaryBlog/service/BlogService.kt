package xyz.kautto.TemporaryBlog.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import xyz.kautto.TemporaryBlog.entity.BlogDto
import xyz.kautto.TemporaryBlog.repository.BlogRepository
import java.time.LocalDateTime

@Service
class BlogService(val blogRepo: BlogRepository, val blogValidator: BlogValidator) {

    var logger: Logger = LoggerFactory.getLogger(BlogService::class.java)

    fun fetchBlogs(): List<BlogDto>  {
        return blogRepo.findAllByOrderByIdDesc()
    }

    fun createBlog(blog: BlogDto): BlogDto {
        if (blogValidator!!.validate(blog)) {
            blogRepo!!.save(blog)
            return blog
        }
        logger.error("Given blog was invalid!")
        return blog
    }

    @Transactional
    fun deleteExpired() {
        val time = LocalDateTime.now()
        blogRepo!!.deleteByExpiresAtBefore(time)
    }
}