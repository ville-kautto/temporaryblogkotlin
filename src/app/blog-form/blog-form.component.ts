import { Component, OnInit } from '@angular/core';
import {BlogService} from "../shared/services/BlogService/blog.service";
import {Blog} from "../shared/models/Blog";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'blog-form',
  templateUrl: './blog-form.component.html',
  styleUrls: ['./blog-form.component.css']
})
export class BlogFormComponent implements OnInit {

  blog: Blog = {
    title: "",
    content: "",
    author: "",
    timeActive: 0
  };

  constructor(private blogService: BlogService) { }

  ngOnInit(): void {
  }

  submit(): void {
    this.blogService.createBlog(this.blog).subscribe(blog => window.location.reload())
  }

}
