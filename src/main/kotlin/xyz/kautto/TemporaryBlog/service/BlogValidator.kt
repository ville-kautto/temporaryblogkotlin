package xyz.kautto.TemporaryBlog.service

import org.springframework.stereotype.Service
import xyz.kautto.TemporaryBlog.entity.BlogDto

@Service
class BlogValidator {
    // Returns true if all fields are valid, otherwise returns false
    fun validate(blog: BlogDto): Boolean {
        return blog.title!!.isNotEmpty() &&
                blog.content!!.isNotEmpty() &&
                blog.author!!.isNotEmpty() &&
                blog.timeActive!! > 0
    }
}