# TemporaryBlogKotlin

A blog site where everybody can post and no messages will remain forever.
This is the Kotlin version of a simple web application, the Java version of the application can be found [here](https://gitlab.com/ville-kautto/temporaryblog)

### Requirements
Running the application requires the following programs installed into your system
- Java 8 or later
- Kotlin 1.5

## Building the application
To build the application, clone the repository and run the following command in the project's root directory.
> mvnw clean install

## Running the application
To start the application, run the following command after building the application.
> mvnw spring-boot:run

The application will then be started and the application can be accessed with a web browser at localhost:8080
