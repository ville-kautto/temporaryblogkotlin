package xyz.kautto.TemporaryBlog.controller

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import xyz.kautto.TemporaryBlog.entity.BlogDto
import xyz.kautto.TemporaryBlog.service.BlogService
import java.time.LocalDateTime


@RestController
class BlogController {
    @RestController
    @RequestMapping("/api")
    class BlogController(val blogService: BlogService) {
        var logger = LoggerFactory.getLogger(xyz.kautto.TemporaryBlog.controller.BlogController::class.java)

        @GetMapping("/blogs")
        @ResponseStatus(HttpStatus.ACCEPTED)
        fun getBlogs(): List<BlogDto> {
            blogService.deleteExpired()
            logger.info("Returning blogs")
            return blogService.fetchBlogs()
        }

        @PostMapping("/blogs")
        @ResponseStatus(HttpStatus.CREATED)
        fun createBlog(@RequestBody blog: BlogDto): ResponseEntity<BlogDto> {
            val time = LocalDateTime.now()
            blog.createdAt = time!!
            blog.expiresAt = setExpirationTime(time, blog.timeActive)
            logger.info(String.format("Blog valid for %s - %s ", blog.createdAt, blog.expiresAt))
            return ResponseEntity(blogService.createBlog(blog), HttpStatus.OK)
        }

        private fun setExpirationTime(time: LocalDateTime, timeActive: Long): LocalDateTime {
            return time.plusMinutes(timeActive)
        }
    }
}