package xyz.kautto.TemporaryBlog.repository

import org.springframework.data.repository.CrudRepository
import xyz.kautto.TemporaryBlog.entity.BlogDto
import java.time.LocalDateTime

interface BlogRepository : CrudRepository<BlogDto, Long> {
    fun findAllByOrderByIdDesc(): List<BlogDto>
    fun deleteByExpiresAtBefore(expiryDate: LocalDateTime)
}
