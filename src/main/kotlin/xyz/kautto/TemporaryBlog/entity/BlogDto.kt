package xyz.kautto.TemporaryBlog.entity

import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class BlogDto (
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(nullable = false, updatable = false)
    val id: Long,

    @Column(nullable = false)
    val title: String,

    @Column(nullable = false)
    val content: String,

    @Column(nullable = false)
    val author: String,
    val timeActive: Long,

    @Column(name = "created_at")
    var createdAt: LocalDateTime?,

    @Column(name = "expires_at")
    var expiresAt: LocalDateTime?
)